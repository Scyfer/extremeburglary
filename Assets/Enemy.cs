using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	
	public bool PlayerDetected = false;
	public Player MPlayer = null;
	public float FOVDegrees= 60;
	public float MaxSightRange = 4f;
	public GameObject ground = null;
	public NavMeshAgent agent = null;
	public float AgroRange = 1.5f;
	
	public string Level = "";
	public AudioSource FreezeSource = null;
	private AudioSource mSource = null;
	
	// Use this for initialization
	void Start () 
	{
		agent = GetComponent<NavMeshAgent>();
		mSource = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update ()
	{
		if (mSource != null)
		{
			float speedPercentage = GetComponent<NavMeshAgent>().velocity.magnitude / GetComponent<NavMeshAgent>().speed;
			mSource.volume = speedPercentage / 2;
		}
		
		if (MPlayer != null)
		{
			CheckIfPlayerInView();
		}
		
		if (PlayerDetected && Vector3.Distance(transform.position, MPlayer.transform.position) < 0.75f)
		{
			GameManager obj = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
			if (obj != null)
			{
				obj.RemoveLife(Level);
			}
		}
		
		if (!PlayerDetected)
		{
			if (agent != null && !agent.hasPath)
			{
				float x = ground.transform.localScale.x;
				float y = ground.transform.localScale.z;
				
				float newX = Random.Range(-4.9f*x, 4.9f*x);
				float newY = Random.Range(-4.9f*y, 4.9f*y);
				//Debug.Log(string.Format("{0} is going to {1},{2}", this.name, newX, newY));
				agent.SetDestination(new Vector3(newX,0f, newY));
			}
		}
		else
		{
			agent.SetDestination(MPlayer.transform.position);
		}
	}
	
	private void CheckIfPlayerInView()
	{
		Vector3 diffPos = MPlayer.transform.position - transform.position;
		diffPos = Vector3.Normalize(diffPos);
		float angle = Mathf.Acos(Vector3.Dot(transform.forward, diffPos));
		float rads = FOVDegrees * Mathf.Deg2Rad;
		if ((angle <= rads) || (Vector3.Distance(MPlayer.transform.position, transform.position) <= AgroRange))
		{
			//Debug.DrawRay(transform.position, diffPos *MaxSightRange, Color.red);
			RaycastHit hit = new RaycastHit();
			if (Physics.Raycast(transform.position, diffPos, out hit, MaxSightRange))
			{
				
				if (hit.transform.gameObject == MPlayer.gameObject)
				{
					if (PlayerDetected == false)
					{
						Debug.Log("Play audio");
						FreezeSource.Play();
					}
					PlayerDetected = true;
				}
				else
				{
					PlayerDetected = false;
				}
			}
		}
	}
}
