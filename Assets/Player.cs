using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	
	public int NumberOfSteals = 0;
	
	public float MoveSpeed = 1f;
	public float CameraRotateSpeed = 2f;
	public float LerpSpeed = 1f;
	public Vector3 MouseSpeed = Vector2.zero;
	public Vector3 AverageSpeed = Vector3.zero;
	
	public KeyCode TurnLeft = KeyCode.Q;
	public KeyCode TurnRight = KeyCode.E;
	
	public bool RightClickDown = false;
	public Camera Cam = null;
	
	public AudioClip StealClip = null;
	private AudioSource mSource = null;
	
	public void Steal()
	{
		NumberOfSteals++;
		if (StealClip != null)
		{
			AudioSource src = Cam.gameObject.GetComponent<AudioSource>();
			src.clip = StealClip;
			//src.Play();
		}
	}
	// Use this for initialization
	void Start () 
	{
		MoveSpeed = GetComponent<NavMeshAgent>().speed;
		mSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mSource != null)
		{
			float speedPercentage = GetComponent<NavMeshAgent>().velocity.magnitude / MoveSpeed;
			mSource.volume = speedPercentage * 0.8f;
		}
		
		if (Input.GetKey(KeyCode.W))
		{
			transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime);
		}
		else if (Input.GetKey(KeyCode.S))
		{
			transform.Translate(Vector3.back * MoveSpeed * Time.deltaTime);
		}
		
		if (Input.GetKey(KeyCode.A))
		{
			transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime);
		}
		else if (Input.GetKey(KeyCode.D))
		{
			transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime);
		}
		
		/*
		if (Input.GetMouseButton(0))
		{
			int layerMask = 1 << 8;
			layerMask = ~layerMask;
			RaycastHit hit = new RaycastHit();
			Ray ray = Cam.ScreenPointToRay(Input.mousePosition);
			Debug.DrawRay(ray.origin, ray.direction *10, Color.red);
			
			if (Physics.Raycast(ray, out hit, 50f, layerMask))
			{
				//Debug.Log(hit.point);
				NavMeshAgent agent = GetComponent<NavMeshAgent>();
				if (agent != null)
				{
					agent.destination = hit.point;
				}
			}
			
		}
		*/
		if (Input.GetMouseButtonDown(1))
		{
			RightClickDown = true;
		}
		if (Input.GetMouseButtonUp(1))
		{
			RightClickDown = false;
		}
		
		if (Input.GetKey(TurnLeft))
		{
			transform.Rotate(new Vector3(0f, -90f, 0f) * Time.deltaTime * CameraRotateSpeed);
		}
		else if (Input.GetKey(TurnRight))
		{
			transform.Rotate(new Vector3(0f, 90f, 0f) * Time.deltaTime * CameraRotateSpeed);
		}		
		
		if (RightClickDown)
		{
			MouseSpeed = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
			AverageSpeed = Vector3.Lerp(AverageSpeed, MouseSpeed, Time.deltaTime * 5);
			
			//float i = Time.deltaTime * LerpSpeed;
			//MouseSpeed = Vector3.Lerp(MouseSpeed, Vector3.zero, i);
			
			transform.Rotate(Cam.transform.up * MouseSpeed.x * CameraRotateSpeed, Space.Self);
			//Cam.transform.Rotate(Cam.transform.right * MouseSpeed.y * CameraRotateSpeed, Space.World);
		}
		
	}
}
