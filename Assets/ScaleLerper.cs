using UnityEngine;
using System.Collections;

public class ScaleLerper : MonoBehaviour
{
	
	public Vector3 StartScale = Vector3.zero;
    public Vector3 EndScale = Vector3.zero;
    public float duration  = 1.0f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		 float lerp = Mathf.PingPong (Time.time, duration) / duration;
         transform.localScale = Vector3.Lerp(StartScale, EndScale, lerp);
	}
}
