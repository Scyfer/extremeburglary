using UnityEngine;
using System.Collections;

public class ColorLerper : MonoBehaviour
{
	
	public Color StartColor;
    public Color EndColor;
    public float duration  = 1.0f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		float lerp = Mathf.PingPong (Time.time, duration) / duration;
     	transform.renderer.material.color = Color.Lerp(StartColor, EndColor, lerp);
	}
}
