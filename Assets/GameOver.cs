using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	// Use this for initialization
	void Start () {
	StartCoroutine(Restart());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public IEnumerator Restart()
	{
		yield return new WaitForSeconds(3f);
		Application.LoadLevelAsync("Master");
	}
}
