using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
	public int MLives = 3;
	// Use this for initialization
	
	void Awake()
	{
		DontDestroyOnLoad(transform.gameObject);
	}
	
	public void RemoveLife(string level)
	{
		MLives--;
		if (MLives <= 0)
		{
			Application.LoadLevel("GameOver");
			Destroy(this.gameObject);
		}
		else
		{
			Application.LoadLevel(level);
		}
	}
}
