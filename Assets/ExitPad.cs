using UnityEngine;
using System.Collections;

public class ExitPad : MonoBehaviour
{
	public Player Player = null;
	public int RequiredSteals = 0;
	public bool Enabled = false;
	public float PadRange = 1f;
	public string LevelToLoad = "GameOver";
	public GameObject ExitText = null;
	
	// Use this for initialization
	void Start ()
	{
		renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Player != null && Player.NumberOfSteals >= RequiredSteals && !Enabled)
		{
			Debug.Log("Come win!");
			renderer.enabled = true;
			Enabled = true;
		}
		
		if (Enabled)
		{
			
			if (ExitText != null)
			{
				ExitText.SetActive(true);
				ExitText.transform.LookAt(Player.Cam.transform);
				ExitText.transform.localEulerAngles = new Vector3(30f, ExitText.transform.localEulerAngles.y, 30f);
			}
			
			if (Vector3.Distance(transform.position, Player.transform.position) < PadRange)
			{
				Application.LoadLevelAsync(LevelToLoad);
			}
		}
	}
}
