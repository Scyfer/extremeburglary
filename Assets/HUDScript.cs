using UnityEngine;
using System.Collections;

public class HUDScript : MonoBehaviour {
	public GUIText PlacesRobbed = null;
	public GUIText Required = null;
	public GUIText Time = null;
	public GUIText Lives = null;
	
	public Player MPlayer = null;
	
	private const string mRobbed = "Places Robbed: ";
	private const string mRequired = "Required: ";
	private const string mTime = "Time Remaining: ";
	
	public float TimeInSeconds = 120;
	public int RequiredSteals = 1;
	public ExitPad ExitPad = null;
	
	// Use this for initialization
	void Start ()
	{
		GameManager obj = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
		if (obj != null)
		{
			Lives.text = "Lives: " + obj.MLives;
		}
		StartCoroutine(Countdown());
		Required.text = string.Format("{0}{1}", mRequired, RequiredSteals);
		ExitPad.RequiredSteals = RequiredSteals;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (MPlayer != null)
		{
			PlacesRobbed.text = string.Format("{0}{1}", mRobbed, MPlayer.NumberOfSteals);
		}
	}
	
	public IEnumerator Countdown()
	{
		float time = TimeInSeconds;
		while (time > 0)
		{
			System.TimeSpan display = System.TimeSpan.FromSeconds(time);
			Time.text = string.Format("{0}{1}:{2:00}", mTime, (int)display.TotalMinutes, display.Seconds);
			time -= 0.1f;
			yield return new WaitForSeconds(0.1f);
		}
		Application.LoadLevelAsync("GameOver");
	}
}
