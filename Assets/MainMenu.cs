using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	
	public int XOffset = 64;
	public int YOffset = 64;
	public int Width = 128;
	public int Height = 128;
	
	public Rect WindowRect = new Rect(0,0,0,0);
	// Use this for initialization
	void Start ()
	{
		WindowRect = new Rect(Screen.width /2 - XOffset, Screen.height / 2 - YOffset, Width, Height);
	}
	
	public void OnGUI()
	{
		WindowRect = GUI.Window(0, WindowRect, CreateWindow, "Main Menu");
	}
	
	public void CreateWindow(int winId)
	{
		if (GUILayout.Button("Start Game"))
		{
			Application.LoadLevelAsync("ObjectiveScreen");
		}
		
		if (GUILayout.Button("Quit"))
		{
			Application.Quit();
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
