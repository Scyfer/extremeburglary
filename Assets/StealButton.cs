using UnityEngine;
using System.Collections;

public class StealButton : MonoBehaviour
{
	
	public bool Stealing = false;
	public bool Stolen = false;
	public float StealRange = 1f;
	public float TimeToSteal = 3f;
	
	public Player MPlayer = null;
	public GameObject DisplayText = null;
	
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (MPlayer != null && !Stolen)
		{
			if (Vector3.Distance(transform.position, MPlayer.transform.position) < StealRange)
			{
				if (!Stealing)
				{
					if (DisplayText != null)
					{
						DisplayText.SetActive(true);
						DisplayText.transform.LookAt(MPlayer.Cam.transform);
						DisplayText.transform.localEulerAngles = new Vector3(30f, DisplayText.transform.localEulerAngles.y, 30f);
						
					}
					Debug.Log("----STEALING----");
					Stealing = true;
					StartCoroutine(StealCoroutine());
				}
			}
			else 
			{
				StopAllCoroutines();
				Stealing = false;
				if (DisplayText != null)
				{
					DisplayText.SetActive(false);
				}
			}
		}
	}
	
	private IEnumerator StealCoroutine()
	{
		float timeToSteal = 0f;
		while (timeToSteal < TimeToSteal)
		{
			timeToSteal += 0.1f;
			yield return new WaitForSeconds(0.1f);
		}
		
		Stolen = true;
		Debug.Log("----STOLEN----");
		MPlayer.Steal();
		IEnumerable enemies = GameObject.FindObjectsOfType(typeof(Enemy));
		int num = 0;
		foreach(Enemy enemy in enemies)
		{
			if (Vector3.Distance(enemy.transform.position, transform.position) <= 30)
			{
				num++;
				enemy.GetComponent<NavMeshAgent>().SetDestination(transform.position);
			}
		}
		Debug.Log(num + " enemies in range");
		StartCoroutine(DisableCoroutine());
		
	}
	
	private IEnumerator DisableCoroutine()
	{
		//gameObject.SetActive(false);
		gameObject.renderer.enabled = false;
		if (DisplayText != null)
		{
			DisplayText.SetActive(false);
		}
		AudioSource src = GetComponent<AudioSource>();
		src.Play();
		
		yield return new WaitForSeconds(10f);
		gameObject.SetActive(false);
	}
}
